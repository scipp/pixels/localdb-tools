from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

SCOPES = 'https://www.googleapis.com/auth/spreadsheets'
sheet_id = '1BrP1ZUhfLx81iy4winwhAItocJb6Eg7z8EUW_kOVk5g'

creds=None

if os.path.exists('token.pickle'):
    with open('token.pickle', 'rb') as token:
        creds = pickle.load(token)
# If there are no (valid) credentials available, let the user log in.
if not creds or not creds.valid:
    if creds and creds.expired and creds.refresh_token:
        creds.refresh(Request())
    else:
        flow = InstalledAppFlow.from_client_secrets_file('credentials.json', SCOPES)
        creds = flow.run_local_server(port=0)
    # Save the credentials for the next run
    with open('token.pickle', 'wb') as token:
        pickle.dump(creds, token)

service = build('sheets', 'v4', credentials=creds)


spreadsheet_id = sheet_id 
sheetname='Summary of Thin Quad' 
range_ = sheetname+"!A1:B10"

# シートの作成
#requests=[]
#requests.append({
#    'addSheet':{
#        "properties":{
#            "title": sheetname,
#            "index": "0",
#            }
#
#        }
#    })
#
#body={'requests':requests}
#response=service.spreadsheets().batchUpdate(spreadsheetId=spreadsheet_id, body=body).execute()
#sheetid=response['replies'][0]['addSheet']['properties']['sheetId']

#セルに文字列を入れる
#v['values']=[
#        [1,  2],
#        [3,  4],
#        [4,  5],
#        [5,  6],
#        [6,  7],
#        [7,  8],
#        [8,  9],
#        [10, 11],
#        [12, 13],
#        ['test', 'This is a test for the spreadsheet'],
#        ]
v={}
v['values'] = [
["hoge"],
["hoge"]
]

range_ = sheetname+"!L3:L4" + str(len(v["values"])+2)
v['range']=range_
v['majorDimension']="ROWS"

value_input_option = 'USER_ENTERED'
insert_data_option='OVERWRITE'
result = service.spreadsheets().values().update( spreadsheetId=spreadsheet_id, range=range_, valueInputOption=value_input_option, body=v).execute()

#requests = []
#requests.append({
#    "updateBorders":{
#        "range": {
#            "sheetId": sheetid,
#            "startRowIndex": 0,
#            "endRowIndex": 1,
#            "startColumnIndex": 0,
#            "endColumnIndex": 2,
#            },
#        "bottom": {
#            "style": "SOLID",
#            "width": "1",
#            "color": { "red": 0, "green":0, "blue":0 },
#            },
#        },
#    })
#
#requests.append({
#    "repeatCell": {
#        "range": {
#            "sheetId": sheetid,
#            "startRowIndex": 0,
#            "endRowIndex": 1,
#            "startColumnIndex": 0,
#            "endColumnIndex": 2,
#            },
#        "cell": {
#            "userEnteredFormat": {
#                "horizontalAlignment" : "LEFT",
#                "textFormat": {
#                    "fontSize": 11,
#                    "bold": True,
#                    "foregroundColor": {
#                        "red": 1.0,
#                        },
#                    }
#                }
#            },
#        "fields": "userEnteredFormat(textFormat,horizontalAlignment)"
#        },
#    })
#body = { 'requests': requests }
#response = service.spreadsheets().batchUpdate(spreadsheetId=spreadsheet_id, body=body).execute()
