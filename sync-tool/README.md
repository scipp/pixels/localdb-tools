# Synchronization tool

## Usage

### Case of usage on local user

```
./setup_sync_tool.sh
```

It will create ./bin/localdbtool-sync.py and `my_configure.yml`, and open to edit `my_configure.yml` automatically. Then type below to see what will be happened.

```
./bin/localdbtool-sync.py --config my_configure.yml
```

If there is no problem, type `y` to continue.

