#!/bin/bash
#################################
# Author: Eunchong Kim
# Email: eunchong.kim at cern.ch
# Date: April 2019
# Project: Local Database for ITk
# Description: Archive mongo data
#################################

# reset the SECONDS
SECONDS=0

# Variables for SLACK app
header="Content-type: application/json"
url="https://hooks.slack.com/services/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"

message_type="ERROR"
message="ARCHIVE FAILED!"
TIME="`date +"%Y-%m-%d %H:%M:%S"`"
data="{\"text\":\"
        [$message_type] $message from $HOSTNAME
        TIME: $TIME
    \"}"

function usage() {
    echo -e "Usage) ./bin/localdbtool-archive.sh [--config <config file>]"
    echo -e "\t-h -help"
    echo -e "\t-f --config       : config path"
    echo -e "\t-p --db-port      : database port (default. 27017)"
    echo -e "\t-d --db-name      : database name (default. localdb)"
    echo -e "\t-a --archive_path : archive path (default. ./archive-mongo-data)"
    echo -e "\t-l --log          : log path (default. ./log)"
    echo -e "\t-n --n_archives   : # of archives to keep (default. 2)"
    exit 1
}

# default parameters
db_port=27017
db_name="localdb"
archive_path="./archive-mongo-data"
log_path="./log"
n_archives=2

while [ ! -z $1 ]; do
#    PARAM=`echo $1 | awk -F= '{print $1}'`
    case $1 in
        -h | --help)
            usage
            ;;
        -f | --config)
            config_path=$2
            shift
            ;;
        -p | --db-port)
            db_port=$2
            shift
            ;;
        -d | --db-name)
            db_name=$2
            shift
            ;;
        -a | --archive-path)
            archive_path=$2
            shift
            ;;
        -l | --log-path)
            log_path=$2
            shift
            ;;
        -n | --n-archives)
            n_archives=$2
            shift
            ;;
        *)
            echo -e "Wrong usage!"
            usage
            ;;
    esac
    shift
done

# Parse from yaml file
function parse_yaml {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("%s%s%s=\"%s\"\n", "'$prefix'",vn, $2, $3);
      }
   }'
}

# Read config file if set
if [ ! -z $config_path ]; then
    echo "The config path set! Read config file ..."
    eval $(parse_yaml $config_path)
fi

echo -e "Confirmation ..."
echo -e " - config_path='$config_path'"
echo -e " - db_port='$db_port'"
echo -e " - db_name='$db_name'"
echo -e " - archive_path='$archive_path'"
echo -e " - log_path='$log_path'"
echo -e " - n_archives='$n_archives'"
echo -e " - Leftovers: $@"
echo -e ""

# config file
echo -e "Continue? [y/n]"
unset answer
read -p "> " answer
while [ -z $answer ]; do
    echo -e "Continue? [y/n]"
    read -p "> " answer
done
echo -e ""
if [ $answer != "y" ]; then
    echo -e "Could not archive data."
    echo -e "Exit!"
    exit
fi

echo -e "Archiving..."

mkdir -p $archive_path
mkdir -p $log_path

dump="./dump_`date +%y%m%d_%H%M%S`"

tool_db_name="${db_name}tools"
mongodump --port $db_port --db $db_name --out $dump --quiet
mongodump --port $db_port --db $tool_db_name --out $dump --quiet

tar zcvf $archive_path/dump_`date +%y%m%d_%H%M%S`.tar.gz $dump >/dev/null \
    && (echo $? && echo -e "Done!" && echo -e "$TIME, succeed : $SECONDS s" >> ${log_path}/localdbtool-archive.log) \
    || (echo $? && curl -X POST -H "$header" --data "$data" $url \
        && echo -e "$RED $message_type $NC $message" \
        && echo -e "$TIME, $message_type $message : $SECONDS s" >> ${log_path}/localdbtool-archive.log \
        && exit 1
    )
rm -rf $dump

# Delete over numbers of archives
nn=$(( $n_archives+1 ))
if test -n "`ls -t ${archive_path}/*.tar.gz | tail -n+$nn`"; then
  rm -v `ls -t ${archive_path}/*.tar.gz | tail -n+$nn`
fi
