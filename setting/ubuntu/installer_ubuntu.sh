#!/bin/bash
#
# author1: Eunchong Kim (eunchong.kim (a) cern.ch)
# date: Dec. 2019
# project: LocalDB
# description: installer for databases

set -e # Exit immediately if a command exits with a non-zero status

# Set log files
date=`date +%y%m%d_%H%M`
stdout_file_name=output_${date}.log
stderr_file_name=stderr_${date}.log
echo -e "Set std log file path: $PWD/${stdout_file_name}\n"
echo -e "Set err log file path: $PWD/${stderr_file_name}\n"


#==================================================================================
#                                       Functions
#==================================================================================
# Usage
function usage() {
    echo -e "Usage) (sudo) bash installer_ubuntu.sh gcc7 yarr ..."
    echo -e "\th help: Show this"
    echo -e "\tall: Install All"
    echo -e "\tcmake cmake3: Install cmake3"
    echo -e "\tgcc7 gcc-7 g++7 g++-7: Install g++ 7"
    echo -e "\tgrafana: Install grafana"
    echo -e "\tinflux influxDB: Install influxDB"
    echo -e "\tlocaldb-tools: Install localdb-tools"
    echo -e "\tmongo mongoDB: Install MongoDB"
    echo -e "\tpython3: Install python3"
    echo -e "\tpip3 modules: Install python3 modules"
    echo -e "\tyarr: Install YARR"
}

# Auto on service
function startService() {
    service_name=$1
    status_name=$2
    service $service_name status | grep -i 'running\|stopped' | awk '{print $5}' | while read output; do
        echo "$output";
        if [ "$output" == "$status_name" ]; then
            service $service_name start
#            echo "$service_name service is UP now.!" | mail -s "$service_name service is DOWN and restarted now On $(hostname)" 2daygeek@gmail.com
        else
            echo "$service_name service is running"
        fi
    done
}

# Compare version
function version_gt() {
    test "$(printf '%s\n' "$@" | sort -V | head -n 1)" != "$1"
}

#==================================================================================
#                               Main
#==================================================================================
# Check Ubuntu version
required_ubuntu_version="16.00"
ubuntu_version=`lsb_release -r | awk {'print $2'}`
if version_gt $required_ubuntu_version $ubuntu_version; then
    echo -e "\e[31m Ubunbu version is lower than required! 16.00. Cannot install! \e[0m"
    exit 1
fi
ubuntu_codename=`lsb_release -c | awk {'print $2'}`

# Check length of arguments
if [ -z $1 ]; then
    echo -e "Wrong usage!"
    usage
    exit 1
fi

# Check sudo if need
if [[ $@ =~ "all" || $@ =~ "cmake" || $@ =~ "gcc" || $@ =~ "g++" || $@ =~ "grafana" || $@ =~ "influx" || $@ =~ "mongo" || $@ =~ "python3" ]]; then
    sudo echo "You are sudo user! Perfect!" || (echo -e "\e[31m You should be sudo user! Try again! \e[0m" && exit 1)
fi


# Set working directory
WORKING_DIR=$PWD


#==================================================================================
#                               Loop for arguments
#==================================================================================
while [ ! -z $1 ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    case $PARAM in
        h | help)
            usage
            exit 0
            ;;

        all)
            if [ `whoami` = root ]; then
                sudo apt update && sudo apt upgrade -y
            fi
            bash $0 cmake3 gcc7 yarr python3 python3-modules mongo localdb-tools influx gragana || (echo -e "\e[31m installer failed! \e[0m" && exit 1)
            ;;

        cmake | cmake3)
            # Check, if no cmake, install default
            if [ `cmake --version` ]; then
                echo "Horray! cmake exist!"
            else
                echo "Oops! no cmake! install cmake..."
                sudo apt install cmake -y
            fi

            # Check version
            required_cmake_version="3.6"
            cmake_version=`cmake --version | grep version | awk {'print $3'} | cut -d. -f1,2`
            # If current cmake version is lower than required, install new
            if version_gt $required_cmake_version $cmake_version; then
                # Install cmake3
                cd /opt
                sudo mkdir cmake3 && cd cmake3
                sudo wget https://github.com/Kitware/CMake/releases/download/v3.16.1/cmake-3.16.1.tar.gz
                sudo tar -zxf cmake-3.16.1.tar.gz
                sudo rm cmake-3.16.1.tar.gz
                cd cmake-3.16.1
                sudo ./bootstrap
                sudo make
                sudo make install
                cd $WORKING_DIR
            fi

            # Test
            echo -e "\n\n###########################################\n\n"
            cmake --version || (echo -e "\e[31m install cmake3 failed! \e[0m" && exit 1)
            echo -e "\n\n###########################################\n\n"
            ;;

        gcc7 | gcc-7 | g++7 | g++-7)
            # Check, if no g++-7, install gcc7
            if [ `g++-7 --version` ]; then
                echo "Horray! g++-7 exist!"
            else
                echo "Oops! no g++-7! install g++-7..."
                sudo apt-get install -y software-properties-common
                sudo add-apt-repository ppa:ubuntu-toolchain-r/test
                sudo apt update
                sudo apt install g++-7 -y
            fi

            # Test
            echo -e "\n\n###########################################\n\n"
            g++-7 --version || (echo -e "\e[31m install gcc7 failed! \e[0m" && exit 1)
            echo -e "\n\n###########################################\n\n"
            ;;

        influx | influxDB)
            # Check, if no influx, install
            if [ `influx --version` ]; then
                echo "Horray! inlux exist!"
            else
                echo "Oops! no influx! install..."
                curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -
                source /etc/lsb-release
                echo "deb https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
                sudo apt-get update && sudo apt-get install influxdb -y
            fi

            # Test
            echo -e "\n\n###########################################\n\n"
            influx --version || (echo -e "\e[31m install influxDB failed! \e[0m" && exit 1)
            echo -e "\n\n###########################################\n\n"
            ;;

        grafana)
            # Check, if no grafana, install
            if [ `grafana-server -v` ]; then
                echo "Horray! grafana exist!"
            else
                echo "Oops! no grafana! install..."
                sudo apt-get install -y libfontconfig1
                wget https://dl.grafana.com/oss/release/grafana_6.5.2_amd64.deb
                sudo dpkg -i grafana_6.5.2_amd64.deb
                rm -f grafana_6.5.2_amd64.deb
                sudo apt update
                sudo apt upgrade -y
            fi

            # Test
            echo -e "\n\n###########################################\n\n"
            grafana-cli --version || (echo -e "\e[31m install grafana failed! \e[0m" && exit 1)
            echo -e "\n\n###########################################\n\n"
            ;;

        localdb-tools)
            # localdb-tools
            if cd localdb-tools; then
                git pull;
            else
                git clone -b devel https://gitlab.cern.ch/YARR/localdb-tools;
            fi
            ;;

        mongo | mongoDB)
            # Check, if no mongo, install
            if [ `mongo --version` ]; then
                echo "Horray! mongo is alreay installed!"
            else
                echo "Oops! no mongo! install..."

                # Pre-requirements for MongoDB
                sudo apt install gnupg -y

                # Install MongoDB
                source /etc/lsb-release
                wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | sudo apt-key add -
                echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu $DISTRIB_CODENAME/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list
                sudo apt update
                sudo apt install -y mongodb-org
            fi

            # Create init if not exist
            if ls /etc/init.d/mongod; then
                echo "Horray!"
            else
                wget https://github.com/mongodb/mongo/raw/master/debian/init.d -O /etc/init.d/mongod
                chmod +x /etc/init.d/mongod
                update-rc.d mongod defaults
                #wget https://github.com/mongodb/mongo/raw/master/debian/mongod.service -O /etc/systemd/system/mongod.service
            fi

            sudo service mongod start

            # Test
            echo -e "\n\n###########################################\n\n"
            mongo --version || (echo -e "\e[31m install mongo failed! \e[0m" && exit 1)
            echo -e "\n\n###########################################\n\n"
            ;;

        python3)
            # Check, if no python, install
            if [ `python3 --version` ]; then
                echo "Horray! python3 is alreay installed!"
            else
                echo "Oops! no python3! install..."
                sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget openssl -y
                sudo apt install python3 python3-pip -y
            fi

            # Check python version
            required_python3_version="3.5"
            python3_version=`python3 -c 'import sys; print(str(sys.version_info[0]) + "." + str(sys.version_info[1]))'`
            # If current python3 version is lower than required, install new
            if version_gt $required_python3_version $python3_version; then
                # Install python3
                cd /opt
                sudo mkdir python3 && cd python3
                sudo wget https://www.python.org/ftp/python/3.7.5/Python-3.7.5.tgz
                sudo tar -xf Python-3.7.5.tgz
                cd Python-3.7.5
                sudo ./configure --enable-optimizations
                sudo make -j4
                sudo make install # altinstall or install, install will overwrite the default system python3 binary

                # Install python3 pip
                sudo curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
                sudo python3 get-pip.py
                sudo pip3 install -U pip
            fi

            # Test
            echo -e "\n\n###########################################\n\n"
            python3 --version || (echo -e "\e[31m install python3 failed! \e[0m" && exit 1)
            pip3 --version || (echo -e "\e[31m install python3 pip failed! \e[0m" && exit 1)
            echo -e "\n\n###########################################\n\n"
            cd $WORKING_DIR
            ;;

        python3-modules)
            # Install python3 modules on local
            if [ `whoami` = root ]; then
                sudo pip3 install arguments coloredlogs Flask Flask-PyMongo Flask-HTTPAuth Flask-Mail pdf2image Pillow prettytable pymongo python-dateutil PyYAML pytz plotly matplotlib numpy requests tzlocal itkdb influxdb || (echo -e "\e[31m install python3 modules failed! \e[0m" && exit 1)
            else
                pip3 install arguments coloredlogs Flask Flask-PyMongo Flask-HTTPAuth Flask-Mail pdf2image Pillow prettytable pymongo python-dateutil PyYAML pytz plotly matplotlib numpy requests tzlocal itkdb influxdb --user || (echo -e "\e[31m install python3 modules failed! \e[0m" && exit 1)
            fi
            ;;

        yarr)
            # Check if yarr repository exist
            if cd YARR; then
                git pull
            else
                # Requirements for YARR
                sudo apt install gnuplot -y
                sudo apt install gnuplot texlive-font-utils -y # instead of texlive-epstopdf
                # sudo apt install zeromq zeromq-devel # Do we need this?
                echo -e "\n\n###########################################\n\n"
                gnuplot --version  || (echo -e "\e[31m install gnuplot failed! \e[0m" && exit 1)
                echo -e "\n\n###########################################\n\n"

                # Install YARR and compile for ubuntu
                git clone -b devel-localdb https://gitlab.cern.ch/YARR/YARR.git yarr
                cd yarr
                mkdir -p build && cd build
                cmake -DCMAKE_CXX_COMPILER=g++-7 ..
                make -j4
                make install
                cd ..
            fi

            # Test
            echo -e "\n\n###########################################\n\n"
            ./bin/scanConsole -h || (echo -e "\e[31m install YARR failed! \e[0m" && exit 1)
            echo -e "\n\n###########################################\n\n"

            cd $WORKING_DIR
            ;;

        test)
            if [ `whoami` = root ]; then
                echo -e "You are sudo user!"
            else
                echo -e "You are not sudo user!"
            fi
            ;;
    esac
    shift
done
