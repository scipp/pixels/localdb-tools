#include <vector>
#include <unistd.h>

#include <TStyle.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TGraph.h>

void SetPlotStyle();

int check() {

    ifstream ifs("speedtest.dat");
    int num = 0;
    int col_n[8] = {419,628,859,797,619,434,402,882};
    ifstream::pos_type cur;

    bool doPrint = false;

    while (true) {
        std::vector<double> v_size[2];
        std::vector<double> v_time[2];
        float file_size = 0; // MB
        float count = 0;
        float tot_time[2] = { 0, 0 };

        ifs.seekg(cur);
        std::string url = "mongodb://";
        while (true) {
            std::string tmp;
            float val;
            cur = ifs.tellg();
            // upload
            ifs >> tmp >> val;
            if (!ifs) break;
            if (tmp=="size") {
                if (file_size==0) {
                    file_size = val;
                    ifs >> tmp >>val;
                    count = val;
                    ifs >> tmp >> tmp;
                    url = url + tmp;
                    ifs >> tmp >> tmp;
                    url = url + ":" + tmp;
                    ifs >> tmp >> tmp;
                    url = url + "/" + tmp;
                    ifs >> tmp >> val;
                } else {
                    break;
                }
            }
            v_time[0].push_back(file_size/val);
            tot_time[0]+=file_size/val;
            // download
            ifs >> tmp >> val;
            v_time[1].push_back(file_size/val);
            tot_time[1]+=file_size/val;
            // dataSize
            ifs >> tmp >> val;
            v_size[0].push_back(val/1000000.);
            // storageSize
            ifs >> tmp >> val;
            v_size[1].push_back(val/1000000.);
        }
        if (file_size==0) break;

        std::cout << url << " (file size: " << file_size << " MB, counts: " << count << ")" << std::endl;

        std::string l[2] = { "upload", "download" };

        for (int i=0; i<2; i++) {
            float ave = tot_time[i] / count;
            std::cout << l[i] << " " << std::to_string(ave) << " Mbps" << std::endl;
        }

        std::cout << std::endl;

        if (doPrint) {
            SetPlotStyle();

            TGraph* graph[2];
            TLegend* leg = new TLegend(0.7,0.5,0.95,0.85);
            leg->SetFillStyle(0);
            leg->SetFillColor(0);
            leg->SetBorderSize(0);
            TCanvas* c1 = new TCanvas("c1","c1",1000,600);
            c1->SetGrid();
            for (int i=0; i<2; i++) {
                graph[i] = new TGraph(v_size[0].size(), &v_size[0][0], &v_time[i][0]);
                std::string title = url + ";storageSize[MB];processSpeed[Mbps]";
                graph[i]->SetTitle(title.c_str());
                graph[i]->SetMarkerStyle(5+2*i);
                graph[i]->SetMarkerSize(0.6);
                graph[i]->SetMarkerColor(col_n[i]);
                graph[i]->SetLineColor(col_n[i]);
                graph[i]->GetYaxis()->SetRangeUser(0, 20);
                leg->AddEntry(graph[i], Form("%s", l[i].c_str()), "p");     

            }
            graph[0]->Draw("APL");
            graph[1]->Draw("PL");
            leg->Draw();
            std::string filename = "data_" + std::to_string(num) + ".png";
            c1->Print(filename.c_str());
            num++;

            delete leg;
            delete c1;
            for (int i=0; i<2; i++) delete graph[i];
        }
    }
    return 0;
}

TStyle* PlotStyle() 
{
  TStyle *Style = new TStyle("Plot","Plot style");

  // use plain black on white colors
  Int_t icol=0; // WHITE
  Style->SetFrameBorderMode(icol);
  Style->SetFrameFillColor(icol);
  Style->SetCanvasBorderMode(icol);
  Style->SetCanvasColor(icol);
  Style->SetPadBorderMode(icol);
  Style->SetPadColor(icol);
  Style->SetStatColor(icol);
  //Style->SetFillColor(icol); // don't use: white fill color for *all* objects

  //Legend
  Int_t ileg=0;
  Style->SetLegendBorderSize(ileg);
  Style->SetLegendFillColor(ileg);
  Style->SetLegendTextSize(0.045);
  Style->SetLegendFont(42);

  // set the paper & margin sizes
  Style->SetPaperSize(20,26);

  // set margin sizes
  Style->SetPadTopMargin(0.10);
  Style->SetPadRightMargin(0.05);
  Style->SetPadBottomMargin(0.16);
  Style->SetPadLeftMargin(0.16);

  // set label offset
  Style->SetLabelOffset(0.01,"xyz");

  // use large fonts
  Int_t font=42; //  helvetica-medium-r-normal "Arial"
  Double_t tsize=0.055;
  Style->SetTextFont(font);

  Style->SetTextSize(tsize);
  Style->SetLabelFont(font,"x");
  Style->SetTitleFont(font,"x");
  Style->SetLabelFont(font,"y");
  Style->SetTitleFont(font,"y");
  Style->SetLabelFont(font,"z");
  Style->SetTitleFont(font,"z");
  
  Style->SetLabelSize(tsize,"x");
  Style->SetTitleSize(tsize,"x");
  Style->SetLabelSize(tsize,"y");
  Style->SetTitleSize(tsize,"y");
  Style->SetLabelSize(tsize,"z");
  Style->SetTitleSize(tsize,"z");

  // use bold lines and markers
  Style->SetMarkerStyle(20);
  Style->SetMarkerSize(2);
  Style->SetHistLineWidth(2.);
  Style->SetLineStyleString(2,"[12 12]"); // postscript dashes
  Style->SetMarkerColor(kAzure+2);
  Style->SetLineColor(kAzure+2);

  // get rid of X error bars (as recommended in ATLAS figure guidelines)
  Style->SetErrorX(0.0001);
  // get rid of error bar caps
  Style->SetEndErrorSize(0.);

  // title(add)
	Style->SetTitleFillColor(0);
	Style->SetFillStyle(0);
	Style->SetTitleStyle(0);
	Style->SetTitleBorderSize(0);

  //Style->SetOptStat(1111);
  Style->SetOptStat(0);
  //Style->SetOptFit(1111);
  Style->SetOptFit(0);

  // put tick marks on top and RHS of plots
  Style->SetPadTickX(1);
  Style->SetPadTickY(1);

  return Style;
}

void SetPlotStyle()
{
  TStyle* plotStyle = 0;
  std::cout << "\nApplying Plot plotting style settings...\n" << std::endl ;
  if ( plotStyle==0 ) plotStyle = PlotStyle();
  gROOT->SetStyle("Plot");
  gROOT->ForceStyle();
  gStyle->SetTickLength(0.02);
}
