#!/bin/bash
##################################
## Author1: Eunchong Kim (eunchong.kim at cern.ch)
## Copyright: Copyright 2019, localDB tools
## Date: Aug. 2019
## Project: Local Database Tools
## Description: Update mongoDB 3.6 to 4.0 for centos
##################################

function usage {
    cat <<EOF

Usage:
    ./upgrade_mongoDB_centos.sh -p <port>

    - h               Show this usage
    - p <port number> Specifies port number user by MongoDB

Option:
    - d <data dir>    Specifies path to data directory of MongoDB (default. /var/lib/mongo)
    - b <backup dir>  Specifies path to backup directory of MongoDB (default ./mongoDB-backup)
EOF
}

data_path=/var/lib/mongo
backup_path=./mongoDB-backup
while getopts hp:d:y OPT
do
    case ${OPT} in
        h ) usage
            exit ;;
        p ) port=${OPTARG} ;;
        d ) data_path=${OPTARG} ;;
        b ) backup_path=${OPTARG} ;;
        * ) usage
            exit ;;
    esac
done

if [ -z ${port} ]; then
    echo "Provide port used by MongoDB under option -p. (default. 27017)"
    exit 1
fi

sudo echo "OK!"
shell_dir=$(cd $(dirname ${BASH_SOURCE}); pwd)

#----------------
# Confirm mongoDB
if which mongo > /dev/null 2>&1; then
    mongo_version=`awk '{print $4}' <<< $( mongo --version )`
    if [ ${mongo_version:0:1} = 'v' ]; then
        major_ver=${mongo_version:1:1}
        minor_ver=${mongo_version:3:1}
    else
        major_ver=${mongo_version:0:1}
        minor_ver=${mongo_version:2:1}
    fi
else
    major_ver=0
    minor_ver=0
fi

RESET=false
sudo systemctl start mongod.service
if [ $? = 0 ]; then
    sleep 5
    mongo --port ${port} --eval db > /dev/null 2>&1
    if [ $? = 0 ]; then
        mongo --port ${port} --eval "db.adminCommand( { setFeatureCompatibilityVersion: '${major_ver}.${minor_ver}' } )"
        if [ ${major_ver} = 4 -a ${minor_ver} = 2  ]; then
            echo "MongoDB version is already 4.2"
            exit 0
        fi
    else
        printf '\033[31m%s\033[m\n' "[ERROR] Failed to connect to MongoDB."
        printf '\033[31m%s\033[m\n' "[ERROR] Maybe port number is wrong."
        printf '\033[31m%s\033[m\n' "[ERROR] Please provide correct port number with option '-p'"
        exit 1
    fi
else
    RESET=true
fi

which yum > /dev/null 2>&1
if [ $? = 1 ]; then
    printf '\033[31m%s\033[m\n' "[ERROR] 'yum' command is required."
    exit 1
fi

if [ ${major_ver} = 0 -a ${minor_ver} = 0 ]; then
    echo ""
    echo "----------------------------------------------"
    printf '\033[33m%s\033[m\n' "[WARNING] MongoDB version 4.2 or greater is required"
    echo "----------------------------------------------"
    echo ""
    echo "Install MongoDB version 4.2? [y/n]"
else
    echo ""
    echo "----------------------------------------------"
    echo "MongoDB version: ${major_ver}.${minor_ver}, port: ${port}"
    printf '\033[33m%s\033[m\n' "[WARNING] MongoDB version 4.2 or greater is required"
    echo ""
    echo "MongoDB Directory Path: ${data_path}"
    if "${RESET}"; then
        printf '\033[33m%s\033[m\n' "[WARNING] Failed to check the featureCompatibilityVersio, will reset it."
    fi
    echo "MongoDB Backup Directory Path: ${backup_path}"
    echo "----------------------------------------------"
    echo ""
    echo "Upgrade MongoDB to 4.2? [y/n]"
fi
unset answer
while [ -z ${answer} ]; do
    read -p "" answer
done
echo -e ""
if [ ${answer} == "y" ]; then
    answer=true
else
    answer=false
fi

if ! "${answer}"; then
    echo "Exit without doing anything."
    exit 0
fi

if ps aux | grep mongod | grep -v grep | grep -v /etc/mongod.conf; then
    echo "--------------------------------"
    echo "Stop these process for updating? [y/n]"
    unset answer
    while [ -z ${answer} ]; do
        read -p "" answer
    done
    echo -e ""
    if [ ${answer} == "y" ]; then
        answer=true
    else
        answer=false
    fi
    if "${answer}"; then
        for pid in $(ps -ef | grep mongod | grep -v grep | grep /etc/mongod.conf | awk '{print $2;}'); do
            sudo kill ${pid}
        done
    else
        echo "Exit without doing anything."
        exit 0
    fi
fi

#----------------------
# Backup mongoDB data directory
#----------------------
sudo systemctl stop mongod.service
if [ -d ${data_path} ]; then
    mkdir -p $backup_path
    sudo tar zcvf ${backup_path}/mongo_`date +%y%m%d_%H%M%S`.tar.gz ${data_path} \
        && (echo $? && echo -e "$TIME, succeed") \
        || (echo $? && echo -e "$TIME, ERROR ARCHIVE FAILED" \
            && exit 1
        )
else
    sudo mkdir -p ${data_path}
    sudo chcon -R -u system_u -t mongod_var_lib_t ${data_path}
    sudo chown -R mongod:mongod ${data_path}
fi

#----------------
# Confirm mongoDB
#----------------
if "${RESET}"; then
    sudo rm -r ${data_path}
    sudo mkdir -p ${data_path}
    sudo chcon -R -u system_u -t mongod_var_lib_t ${data_path}
    sudo chown -R mongod:mongod ${data_path}
fi
if [ ${major_ver} = 0 -a ${minor_ver} = 0 ]; then
    major_ver=4
    minor_ver=0
fi
while ! [ ${major_ver} = 4 -a ${minor_ver} = 2 ]; do
    if [ ${minor_ver} = 6 ]; then
        major_ver=$(( major_ver + 1 ))
        minor_ver=0
    else
        minor_ver=$(( minor_ver + 2 ))
    fi
    sudo rm -f /etc/yum.repos.d/mongodb-org-*.repo
    sudo cp ${shell_dir}/mongodb-org.repo /etc/yum.repos.d/mongodb-org-${major_ver}.${minor_ver}.repo
    sudo sed -i -e "s!MAJORVER!${major_ver}!g" /etc/yum.repos.d/mongodb-org-${major_ver}.${minor_ver}.repo
    sudo sed -i -e "s!MINORVER!${minor_ver}!g" /etc/yum.repos.d/mongodb-org-${major_ver}.${minor_ver}.repo
    sudo yum clean all -y
    sudo yum remove mongodb-org -y
    sudo yum install mongodb-org -y

    sudo chcon -R -u system_u -t mongod_var_lib_t ${data_path}
    sudo chown mongod:mongod -R ${data_path}
    sudo systemctl start mongod.service
    if [ $? = 0 ]; then
        sleep 5
        mongo --port ${port} --eval "db.adminCommand( { setFeatureCompatibilityVersion: '${major_ver}.${minor_ver}' } )"
    else
        sudo rm -r ${data_path}
        sudo mkdir -p ${data_path}
        sudo chcon -R -u system_u -t mongod_var_lib_t ${data_path}
        sudo chown -R mongod:mongod ${data_path}
    fi
    sudo systemctl stop mongod.service
done

if which mongo > /dev/null 2>&1; then
    mongo_version=`awk '{print $4}' <<< $( mongo --version )`
    if [ ${mongo_version:0:1} = 'v' ]; then
        major_ver=${mongo_version:1:1}
        minor_ver=${mongo_version:3:1}
    else
        major_ver=${mongo_version:0:1}
        minor_ver=${mongo_version:2:1}
    fi
else
    major_ver=0
    minor_ver=0
fi

if [ ${major_ver} = 4 -a ${minor_ver} = 2  ]; then
    echo "Success! Upgraded MongoDB version to 4.2!"
    echo "You can start mongodb service by"
    echo "    sudo systemctl start mongod.service"
    echo "Enjoy!"
    exit 0
else
    printf '\033[31m%s\033[m\n' '[ERROR] Something failed. Check "/var/log/mongodb/mongod.log"'
    exit 1
fi
