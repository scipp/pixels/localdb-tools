#!/usr/bin/env python3
#################################
# Author: Arisa Kubota
# Email: arisa.kubota at cern.ch
# Date: Feb 2019
# Project: Local Database for YARR
# Description: DB scheme convertor
# Usage: python app.py --config conf.yml
#################################

### Import
from pymongo import MongoClient
import json

### Set DBs
url = "mongodb://127.0.0.1:27017"
client = MongoClient(url)
dbs = client.list_database_names()
filename = "dbStatus.log"
log_file = open(filename, "w")
for db in dbs:
    this = client[db]
    dbStats = this.command("dbstats")
    json.dump(dbStats, log_file, indent=4)
log_file.close()
