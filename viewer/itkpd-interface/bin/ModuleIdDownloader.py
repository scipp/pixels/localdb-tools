#!/usr/bin/env python3
# -*- coding: utf-8 -*
######################################################
## Author1: Hiroki Okuyama (hiroki.okuyama at cern.ch)
## Copyright: Copyright 2019, ldbtools
## Date: Nov. 2019
## Project: Local Database Tools
## Description: ITkPD Interface
######################################################

import os, sys

sys.path.append(os.path.join(os.path.dirname(__file__), "../.."))
from functions.imports import *

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from download_QC_info import *

####################################
## download of component information


def download():

    code1 = os.environ.get("ITKDB_ACCESS_CODE1", " ")
    code2 = os.environ.get("ITKDB_ACCESS_CODE2", " ")
    token = process_request(code1, code2)
    if token == 0:
        sys.exit(1)
    else:
       ModuleDownloader(code1,code2).download_QC_info(args.component_id) 

if __name__ == "__main__":
    download()
