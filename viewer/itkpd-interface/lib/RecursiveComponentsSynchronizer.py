import os, sys
import PDInterface

import pprint
import dateutil.parser

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from functions.imports import *

class RecursiveComponentsSynchronizer(PDInterface.PDInterface):
    # private
    def __query_institution(self,doc):

        query = {"code": doc["institution"]["code"]}
        if localdb.institution.find_one(query) == None:
            institution_info = {
                "code": doc["institution"]["code"],
                "institution": doc["institution"]["name"],
                "address": "...",
                "HOSTNAME": "...",
                "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
                "dbVersion": dbv,
            }
            localdb.institution.insert_one(institution_info)
        address = localdb.institution.find_one(query)

        return address

    def __create_component_doc(self, doc, address, chipId=-1, children=-1):
        componentType = str(doc["componentType"]["name"]).lower().replace(" ", "_")
        chipType = ""
        if componentType == "module":
            for prop in doc["properties"]:
                if prop["code"] == "FECHIP_VERSION":
                    chipType = prop["value"]
        else:
            chipType = doc["type"]["name"]

        component_doc = {
            "_id": ObjectId(doc["id"]),
            "name": doc["serialNumber"],
            "chipType": chipType,
            "serialNumber": doc["serialNumber"],
            "chipId": chipId,
            "componentType": componentType,
            "address": str(address["_id"]),
            "children": children,
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
            "user_id": -1,
            "proID": doc["code"],
            "proDB": True,
        }

        return component_doc


    def __create_cpr_doc(self, module_doc, child_doc ):

        cpr_doc = {
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
            "parent": str(module_doc["_id"]),
            "child": str(child_doc["_id"]),
            "chipId": child_doc["chipId"],
            "status": "active",
        }

        return cpr_doc
        

    def __create_QC_status_doc(self, cpt_doc):
        stage_flow = []
        stage_vs_test = {}
        stage_map = {}
        test_item_map = {}

        excepCodes = { "MODULE" : [ "READOUT_IN_BASIC_ELECTRICAL_TEST", "TUNING", "BUMP_BOND_QUALITY", "SENSOR_IV" ],
                       "BARE_MODULE" : [],
                       "PCB" : [],
                       "FE_CHIP": []
        }
        temperatures = { "MODULE" : [30, 20, -15] }
        
        for stage in cpt_doc["stages"]:
            stage_map[stage["code"]] = stage["name"]
            test_items = []
            if "testTypes" in stage:
                
                if stage['testTypes'] == None:
                    continue
                
                for testType in stage["testTypes"]:
                    code = testType["testType"]["code"]
                    name = testType["testType"]["name"]
                    
                    # Exception case: multiple temperature TestRuns per Test
                    if code in excepCodes[ cpt_doc['code'] ]:
                        for T in temperatures[ cpt_doc['code'] ]:
                            code_var = code + "_{}{}DEG".format( "MINUS" if T < 0 else "", abs(T) )
                            name_var = name + " ({} degC)".format( T )
                            test_item_map[code_var] = name_var
                            test_items.append( code_var )
                    else:
                        # 1 TestRun per Test
                        test_item_map[code] = name
                        test_items.append( code )
            
            stage_flow.append(stage["code"])
            stage_vs_test[stage["code"]] = test_items

        local_cpt_doc = {
            "code": cpt_doc['code'],
            "name": cpt_doc['name'],
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
            "proddbVersion": proddbv,
            "stage_flow":stage_flow,
            "stage_test":stage_vs_test,
            "stages":stage_map,
            "test_items":test_item_map
        }

        return local_cpt_doc

    def __create_component_status_doc(self, cpt_type, cpt_doc):
        
        # Below is the skeleton
        doc = {
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion"          : dbv,
            "proddbVersion"      : proddbv,
            "componentType"      : cpt_type,
            "component"          : cpt_doc["id"],
            "currentStage"       : cpt_doc["currentStage"]["code"],
            "latestSyncedStage"  : cpt_doc["currentStage"]["code"],
            "status"             : "created",
            "rework_stage"       : [],
            "QC_results"         : {},
            "QC_results_pdb"     : {}
        }

        chipType = ""
        try:
            for prop in cpt_doc["properties"]:
                if prop["code"] == "FECHIP_VERSION":
                    chipType = prop["value"]
                    break
        except Exception as e:
            logger.warning( '{}. Try an alternative method'.format(e) )
            try:
                chipType = cpt_doc['type']['code']
            except Exception as e:
                logger.warning( '{}. Try an alternative method'.format(e) )
                exit(1)

        stageVersion = {}
        
        # Checkout the QC stage-test flow from localdbtools.QC.status
        qcStatus = userdb.QC.status.find_one( { "code":cpt_doc["componentType"]["code"] } )
        if qcStatus == None:
            logger.error( "localdbtools.QC.status does not contain a QC status skeleton for {}".format( cpt_doc["componentType"]["code"] ) )
            
            
        # Fetch 
        for stage in qcStatus["stage_flow"]:
            tests = qcStatus["stage_test"][stage]
            doc["QC_results"][stage] = {}
            doc["QC_results_pdb"][stage] = {}
            for test in tests:
                doc["QC_results"][stage][test] = "-1"
                doc["QC_results_pdb"][stage][test] = "-1"
        
        return doc


    def __downloadStagesAndTests(self, cpt_doc):

        # create an example of a document of QC status
        if userdb.QC.status.find_one( {"proddbVersion":proddbv, "code":cpt_doc["code"] }) == None:
            userdb.QC.status.insert_one( self.__create_QC_status_doc(cpt_doc) )
        else:
            userdb.QC.status.remove( {"proddbVersion":proddbv, "code":cpt_doc["code"] })
            userdb.QC.status.insert_one( self.__create_QC_status_doc(cpt_doc) )

        logger.info("RecursiveComponentsSynchronizer: Downloaded info of stages and QC tests for {}".format( cpt_doc['code'] ) )
        
        return 0


    def __checkTypeAndSerialNumber(self, d):

        #logger.info("Module document ID: " + d["code"])
        if d["serialNumber"] != None and d["type"]["name"] != None :
            #logger.info("Appended in a downloading module list.")
            return True
        else:
            # logger.warning("This module has an old type. Skip downloading.")
            return False

        return False

    def __isFeChipsValid(self, module_doc):
        for child_info in module_doc["children"]:
            if child_info["componentType"]["code"] == "BARE_MODULE":
                if child_info["component"] == None:
                    return False
                else:
                    child_doc = super().getCompFromProdDB(child_info["component"]["code"])
                    for child_child_info in child_doc["children"]:
                        if child_child_info["componentType"]["code"] == "FE_CHIP":
                            if child_child_info["component"] == None:
                                return False
                            if child_child_info["component"]["dummy"]:
                                return False

        return True

    
    def __createPropDocfromAttachment(self, cpt_prod_doc):
        cache_dir = "{}/prop_attachments".format(".")
        try :
            os.makedirs(cache_dir)
        except Exception as e:
            logger.warning( '{}'.format( e ) )
            
        prop_docs = []
        for attachment in cpt_prod_doc["attachments"]:
            if "detail.json" in attachment["filename"]:
                jfile = self.pd_client.get("uu-app-binarystore/getBinaryData",json={"code": attachment["code"]}).content
                with open(os.path.join(cache_dir,"{}".format(attachment["filename"])),"wb") as f:
                    f.write(jfile)
                prop_doc = {}
                with open(os.path.join(cache_dir,"{}".format(attachment["filename"])),"r") as f:
                    prop_doc = json.load(f)
                prop_doc["_id"] = ObjectId(prop_doc["_id"])
                prop_doc["sys"]["mts"] = dateutil.parser.parse(prop_doc["sys"]["mts"])
                prop_doc["sys"]["cts"] = dateutil.parser.parse(prop_doc["sys"]["cts"])
                
                prop_docs.append(prop_doc)

        shutil.rmtree(cache_dir)
        return prop_docs


    def __downloadProperties(self, cpt_prod_doc):
        
        prop_docs = self.__createPropDocfromAttachment(cpt_prod_doc)
        
        for prop_doc in prop_docs:
            pprint.pprint( prop_doc )
            try:
                super().insertDocToLdb("QC.module.prop", prop_doc)
            except:
                logger.info("This module property already exists in LocalDB.")
            try:
                stage_result = "QC_properties."  + prop_doc["testType"]
                localdb.QC.prop.status.update({"component":str(prop_doc["component"]),"proddbVersion":proddbv},{"$set":{ stage_result: str(prop_doc["_id"]) }}  )
            except:
                logger.info("Sign off list does not updated.")
                
        logger.info("RecursiveComponentsSynchronizer: finished downloading test properties\n")
        
        return

    def __createTestDocfromAttachment(self, pd_tr_doc, ldb_tr_doc):
        compointId = ldb_tr_doc["component"]
        cache_dir = "{}/attachments".format( CACHE_DIR )
        try :
            os.makedirs(cache_dir)
        except Exception as e:
            logger.warning( '{}'.format( e ) )
        
        isFoundAttachment = False

        # loop over attachments
        for attachment in pd_tr_doc["attachments"]:
            
            if attachment["filename"] == pd_tr_doc["testType"]["code"] + "_results.json":

                isFoundAttachment = True
                
                for i in range(3):
                    try:
                        jfile = self.pd_client.get("uu-app-binarystore/getBinaryData",json={"code": attachment["code"]}).content
                        with open(os.path.join(cache_dir,"{}".format(attachment["filename"])),"wb") as f:
                            f.write(jfile)
                        break
                    except Exception as e:
                        logger.warning( '{}'.format(e) )
                
                tmp_doc = {}
                with open(os.path.join(cache_dir,"{}".format(attachment["filename"])),"r") as f:
                   tmp_doc = json.load(f)

                tmp_doc["sys"]["mts"] = dateutil.parser.parse(tmp_doc["sys"]["mts"])
                tmp_doc["sys"]["cts"] = dateutil.parser.parse(tmp_doc["sys"]["cts"])
                   
                tmp_doc["_id"] = ObjectId(ldb_tr_doc["_id"])
                tmp_doc["component"] = compointId
                tmp_doc["testType"]  = self.testTypeLocal

                ldb_tr_doc = tmp_doc

                break

        if not isFoundAttachment:
            logger.warning( 'RecursiveComponentsSynchronizer.__createTestDocfromAttachment(): not found attachment file for {}'.format( pd_tr_doc['testType']['code'] ) )
            
        self.__downloadProperties( pd_tr_doc )
            
        shutil.rmtree(cache_dir)
        return ldb_tr_doc
        

    def __getUserName(self, user_info):
        if user_info["middleName"]: return user_info["firstName"] + user_info["middleName"] + user_info["lastName"]
        else: return user_info["firstName"] + user_info["lastName"]
        

    def __syncTestRun( self, componentId, tr_runNumber, pd_tr_doc ):
        
        # self.testTypeLocal: e.g. "SENSOR_IV_30DEG"
        # tr_runNumber: LocalDB ObjectId string of the TestRun
        # pd_tr_doc: ProdDB TestRun document

        try:
            ldb_tr_doc = {
                "_id"          : ObjectId( pd_tr_doc['runNumber'] ),
                "component"    : componentId,
                "user"         : self.__getUserName( pd_tr_doc["user"] ),
                "address"      : pd_tr_doc["institution"]["name"],
                "currentStage" : pd_tr_doc["components"][0]['testedAtStage']['code'],
                "sys"          : {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
                "dbVersion"    : dbv,
                "testType"     : self.testTypeLocal,
                "results"      : {}
            }
        except Exception as e:
            logger.warning( '{}'.format( e ) )
            logger.error( 'failed in constructing LocalDB TestRun format' )
        
        result_doc = self.__createTestDocfromAttachment( pd_tr_doc, ldb_tr_doc )

        localdb.QC.result.remove( { "_id":ObjectId( pd_tr_doc['runNumber'] ) } )
        
        super().insertDocToLdb("QC.result", result_doc)        
        
        return
        
        
    def __bookComponent(self, cpt_type, cpt_sn, cpt_doc):
        logger.info( 'RecursiveComponentsSynchronizer: Attempt to register {} {}'.format( cpt_type, cpt_sn ) )
        
        # QC status skeleton
        cpt_qc_doc = self.__create_component_status_doc( cpt_type, cpt_doc )
        
        # Attempt to get the component record from LocalDB
        cpt_localdb_doc = self.getCompFromLocalDB( cpt_doc["serialNumber"] )
        
        # If the record is blank, insert one
        if cpt_localdb_doc == None:
            address = self.__query_institution( cpt_doc )
            cpt_localdb_doc = self.__create_component_doc( doc=cpt_doc, address=address )
            self.component_list.append( cpt_localdb_doc )
        else:
            logger.info( 'RecursiveComponentsSynchronizer: {} {} is already registered.'.format( cpt_type, cpt_sn ) )
        
        # Synchronize the QC status to ProdDB
        
        # Set the sign-off record of the module up to the current stage
        ldb_componentId           = str( cpt_localdb_doc["_id"] )
        ldb_component_QC_info     = super().getQmsFromLocalDB( ldb_componentId )

        try:
            stageFlow = self.stageFlows[ cpt_type ]
        except:
            logger.error( '__bookComponent(): unknown cpt_type: {}'.format( cpt_type ) )
            
            
        #pprint.pprint( stageFlow )
        currentStage = cpt_doc['currentStage']['code']
        
        if not 'upload_status' in cpt_qc_doc:
            cpt_qc_doc['upload_status'] = {}
            for s in stageFlow:
                cpt_qc_doc['upload_status'][s] = "-1"
                
        for stage in stageFlow:
            
            if stage == currentStage:
                break
            
            cpt_qc_doc['upload_status'][stage] = "1"

        
        # Fetch only ready-state TestRuns
        for i in range(3):
            try:
                pd_testRuns = [ tr for tr in self.pd_client.get("listTestRunsByComponent", json={"component":cpt_doc['code']} ) if tr['state'] == 'ready' ]
                break
            except Exception as e:
                logger.warning( '{}'.format( e ) )
        
        # Loop over TestRuns
        for testRun in pd_testRuns:
            stage        = testRun['stage']['code']
            testType     = testRun['testType']['code']
            
            for i in range(3):
                try:
                    tr_doc       = self.pd_client.get("getTestRun", json={ "testRun":testRun["id"] } )
                    tr_runNumber = testRun['runNumber']
                    break
                except Exception as e:
                    logger.warning( '{}'.format( e ) )
            
            env_temp = -9999
            props = tr_doc['properties']
            for prop in props:
                if prop['code'] == "ENVIRONMENT_TEMPERATURE":
                    env_temp = int( prop['value'] )
                    
            if env_temp != -9999:
                self.testTypeLocal = '{}_{}{}DEG'.format( testType, "MINUS" if env_temp<0 else "", abs(env_temp) )
            else:
                self.testTypeLocal = testType
            
            cpt_qc_doc["QC_results"][stage][self.testTypeLocal]     = tr_runNumber
            cpt_qc_doc["QC_results_pdb"][stage][self.testTypeLocal] = testRun["id"]
            
            logger.info( 'RecursiveComponentsSynchronizer: Added TestRun {}.{} / {}'.format( stage, self.testTypeLocal,  testRun['id'] ) )
            
            # If the test record of tr_runNumber is already present in LocalDB, it is the original, skip.
            # Otherwise, download the prodDB test record to LocalDB.
            
            try:
                localTestRunRecord = localdb.QC.result.find_one( { "_id":ObjectId( tr_runNumber ) } )
                
                self.__syncTestRun( ldb_componentId, tr_runNumber, tr_doc )
                    
                # attach the prodDB testRun record
                localdb.QC.result.update( { "_id":ObjectId( tr_runNumber ) }, { "$set" : { 'prodDB_record' : tr_doc } } )
            except Exception as e:
                logger.warning( '{}'.format( e ) )
            
            
            
        # pprint.pprint( cpt_qc_doc )
        # Remove old record
        localdb.QC.module.status.remove(  {"component": cpt_doc["id"], "proddbVersion":proddbv}  )
        
        # Insert a new record
        localdb.QC.module.status.insert_one( cpt_qc_doc )
        
        logger.info( "RecursiveComponentsSynchronizer: Created QC status doc for {} {}.".format( cpt_type, cpt_sn ) )
        
        return cpt_localdb_doc
        
        
    def __loopSubComponents( self, cpt_doc, cpt_localdb_doc ):
        
        if cpt_doc["children"] == None:
            return
        
        for child_cpt_metadata in cpt_doc["children"]:
            
            # Check which child component type
            child_type = child_cpt_metadata['componentType']['code']
            
            # Skip carrier
            if child_type.lower().find( 'carrier' ) >=0 :
                continue
                
            if child_type.lower().find( 'sensor_tile' ) >=0 :
                continue
                
            child_id = child_cpt_metadata['component']['code']
            
            # Acquire the child component from ProdDB
            child_doc = self.getCompFromProdDB( child_cpt_metadata['component']['code'] )
            child_cpt_sn = child_doc['serialNumber']
            
            self.cpt_sn.append( child_cpt_sn )
            
            child_cpt_localdb_doc = self.__bookComponent( child_type, child_cpt_sn, child_doc )
            
            # Create a new child-parent relation
            
            # For FE chips, make an additional direct link between the chip and the parent module
            if child_type == 'FE_CHIP':
                
                try:
                    cpr_info = self.__create_cpr_doc( self.parent_module_localdb_doc, child_cpt_localdb_doc )
                    
                    if super().getCprFromLocalDB( str( self.parent_module_localdb_doc["_id"]), str( child_cpt_localdb_doc["_id"]) ) == None:
                        self.cpr_list.append(cpr_info)
                        logger.info( 'RecursiveComponentsSynchronizer: {} {} cpr relation was created.'.format( child_type, child_cpt_sn ) )
                    else:
                        logger.info( 'RecursiveComponentsSynchronizer: {} {} cpr relation is already registered.'.format( child_type, child_cpt_sn ) )
                        
                except Exception as e:
                    logger.warning( '{}'.format( e ) )
                    pass
                
            
            # The following generic linking applies to all sub-components
            cpr_info = self.__create_cpr_doc( cpt_localdb_doc, child_cpt_localdb_doc )
            
            if super().getCprFromLocalDB( str( cpt_localdb_doc["_id"]), str( child_cpt_localdb_doc["_id"]) ) == None:
                self.cpr_list.append(cpr_info)
                logger.info( 'RecursiveComponentsSynchronizer: {} {} cpr relation was created.'.format( child_type, child_cpt_sn ) )
            else:
                logger.info( 'RecursiveComponentsSynchronizer: {} {} cpr relation is already registered.'.format( child_type, child_cpt_sn ) )
            
                
            # Recursively apply subcomponents loop
            self.__loopSubComponents( child_doc, child_cpt_localdb_doc )
                
        
        
            
    # public
    def exec(self, component_id=None, serialFilter=None):
        
        # Activate processing indication
        if not os.path.exists(IF_DIR):
            os.makedirs(IF_DIR)
        do_filepath = IF_DIR + '/doing_download.txt'
        with open(do_filepath, 'w') as f:
            f.write('doing now')
        
        
        # Task:
        # 1. list up components to download
        # 2. trace-down all sub-components tree
        # 3. register components and all parent-child relations

        ######################################
        ## Step-1
        ## list up components to download
        
        self.componentIDs = { "MODULE"      :"5bb1e1bef981520009c54bc5",
                              "BARE_MODULE" :"5f1d2df071645a000b7b2c85",
                              "PCB"         :"5f5051fd7951dc000a2da3c1",
                              "FE_CHIP"     :"5a09ab9788792100056f2145"   }
        
        self.stageFlows = {}

        for cpt_name, cpt_id in self.componentIDs.items():
            cpt_doc = self.pd_client.get("getComponentType",json={"id":cpt_id})
            self.__downloadStagesAndTests(cpt_doc)

            self.stageFlows[cpt_name] = []
            for stage in cpt_doc['stages']:
                self.stageFlows[cpt_name].append( stage['code'] )
            
    
        d_cpts = {}
        
        for cpt_name, cpt_id in self.componentIDs.items():
            
            if cpt_name == "FE_CHIP": continue
            
            d_cpt = []
        
            doc = self.pd_client.get('listComponents', json={"project":"P", "componentType":cpt_name } )
            
            for d in doc:
                if self.__checkTypeAndSerialNumber(d):
                    
                    if component_id != None:
                        if component_id in d['serialNumber']:
                            d_cpt.append(d["serialNumber"])
                        
                    else:
                        d_cpt.append(d["serialNumber"])
                        
                if len( d_cpt ) > 5:
                    break
                    
            d_cpts[cpt_name] = d_cpt
            
            logger.info( "RecursiveComponentsSynchronizer: identified {} {} components to be downloaded in total".format( len( d_cpt ), cpt_name ) )
        
        
        ######################################
        ## Step-2
        ## Trace-down all sub-components tree
        
        self.component_list = []
        self.cpr_list = []
        self.cpt_sn = []
        
        for cpt_type, cpts in d_cpts.items():
            
            for this_cpt_sn in cpts:
                
                # Acquire the component doc from ProdDB
                this_cpt_doc = super().getCompFromProdDB( this_cpt_sn )
                
                
                # In case of MODULE, skip if chips are incomplete or blank
                if cpt_type == "MODULE":
                    if not self.__isFeChipsValid( this_cpt_doc ):
                        continue
                
                this_cpt_localdb_doc = self.__bookComponent( cpt_type, this_cpt_sn, this_cpt_doc )
                
                if cpt_type == "MODULE":
                    self.parent_module_doc         = this_cpt_doc
                    self.parent_module_sn          = this_cpt_sn
                    self.parent_module_localdb_doc = this_cpt_localdb_doc
                    
                # Loop over sub-components
                self.__loopSubComponents( this_cpt_doc, this_cpt_localdb_doc )
                        
            # end for this_cpt_sn in cpts:
        # end for cpt_type, cpts in d_cpts.items():
        
        
        ######################################
        ## Step-3
        ## register components and all parent-child relations
        
        for component_item in self.component_list:
            try:
                localdb.component.insert(component_item)
            except:
                logger.info("RecursiveComponentsSynchronizer: This module's serial number has been changed for some reason.\n")
                old_component_name = localdb.component.find_one({"_id":ObjectId(component_item["_id"])})["name"]
                localdb.component.update( {"_id" : ObjectId( component_item["_id"] ) },
                                          {"$set": { "name"        : component_item["name"], 
                                                     "serialNumber": component_item["serialNumber"],
                                                     "proID"       : component_item["proID"]          } } )
                
                localdb.componentTestRun.update_many( {"name":old_component_name}, {"$set": {"name": component_item["name"] } } )
                
                if component_item["componentType"] == "front-end_chip":
                    localdb.chip.update( {"_id":old_component_name}, {"$set": {"name": component_item["name"]} } )
                                
        if len(self.cpr_list)>0:
            logger.info( "RecursiveComponentsSynchronizer: Inserting child-parent relations:" )
        for cpr in self.cpr_list:
            logger.info( 'RecursiveComponentsSynchronizer: parent: {}, child: {}'.format( cpr['parent'], cpr['child'] ) )
        if len(self.cpr_list):
            localdb.childParentRelation.insert_many(self.cpr_list)
            
        # Remove processing indication
        if os.path.exists(IF_DIR + '/doing_download.txt'):
            os.remove(IF_DIR + '/doing_download.txt')
        
        return self.cpt_sn
        
