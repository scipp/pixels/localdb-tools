import os, sys
import PDInterface

sys.path.insert(0, os.path.join(os.path.dirname(os.path.abspath(__file__)), "../lib"))
from functions.imports import *

class ModuleDownloader(PDInterface.PDInterface):
    # private
    def __query_institution(self,doc):

        query = {"code": doc["institution"]["code"]}
        if localdb.institution.find_one(query) == None:
            institution_info = {
                "code": doc["institution"]["code"],
                "institution": doc["institution"]["name"],
                "address": "...",
                "HOSTNAME": "...",
                "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
                "dbVersion": dbv,
            }
            localdb.institution.insert_one(institution_info)
        address = localdb.institution.find_one(query)

        return address

    def __create_component_doc(self, doc, address, chipId=-1, children=-1):
        componentType = str(doc["componentType"]["name"]).lower().replace(" ", "_")
        chipType = ""
        if componentType == "module":
            for prop in doc["properties"]:
                if prop["code"] == "FECHIP_VERSION":
                    chipType = prop["value"]
        else:
            chipType = doc["type"]["name"]

        component_doc = {
            "_id": ObjectId(doc["id"]),
            "name": doc["serialNumber"],
            "chipType": chipType,
            "serialNumber": doc["serialNumber"],
            "chipId": chipId,
            "componentType": componentType,
            "address": str(address["_id"]),
            "children": children,
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
            "user_id": -1,
            "proID": doc["code"],
            "proDB": True,
        }

        return component_doc


    def __create_cpr_doc(self, module_doc, child_doc, map_):

        cpr_doc = {
#            "_id": ObjectId(map_[str(child_doc["_id"])]),
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
            "parent": str(module_doc["_id"]),
            "child": str(child_doc["_id"]),
            "chipId": child_doc["chipId"],
            "status": "active",
        }

        return cpr_doc

    def __create_QC_status_doc(self, cpt_doc):
        stage_flow = []
        stage_vs_test = {}
        stage_map = {}
        test_item_map = {}

        for stage in cpt_doc["stages"]:
            stage_map[stage["code"]] = stage["name"]
            test_items = []
            if "testTypes" in stage:
                for testType in stage["testTypes"]:
                    test_item_map[testType["testType"]["code"]] = testType["testType"]["name"]
                    test_items.append(testType["testType"]["code"])
            stage_flow.append(stage["code"])
            stage_vs_test[stage["code"]] = test_items

        cpt_doc = {
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion": dbv,
            "proddbVersion": proddbv,
            "stage_flow":stage_flow,
            "stage_test":stage_vs_test,
            "stages":stage_map,
            "test_items":test_item_map
        }

        return cpt_doc

    def __create_module_status_doc(self, module_doc):
        doc = {
            "sys": {"mts": datetime.utcnow(), "cts": datetime.utcnow(), "rev": 0},
            "dbVersion"          : dbv,
            "proddbVersion"      : proddbv,
            "component"          : module_doc["id"],
            "currentStage"       : module_doc["currentStage"]["code"],
            "latestSyncedStage"  : module_doc["currentStage"]["code"],
            "status"             : "created",
            "rework_stage"       : [],
            "QC_results"         : {}
        }
        for stage, tests in self.stage_test_map.items():
            doc["QC_results"][stage] = {}
            for test in tests:
                doc["QC_results"][stage][test["ldb_testType"]] = "-1"
        return doc


    def __downloadStagesAndTests(self, module_cpt_doc):

        # create an example of a document of QC status
        logger.info("Start downloading info of stages and QC tests")
        if userdb.QC.status.find_one({"proddbVersion":proddbv}) == None:
            userdb.QC.status.insert_one( self.__create_QC_status_doc(module_cpt_doc) )
        logger.info("Finished!!\n")

        return 0


    def __checkTypeAndSerialNumber(self, d):

        logger.info("Module code: " + d["code"])
        if d["serialNumber"] != None and d["type"]["name"] != None :
            logger.info("Appended in a downloading module list.")
            return True
        else:
            logger.warning("This module has an old type. Skip downloading.")
            return False

        return False

    def __checkFEchips(self, module_doc):
        for child_info in module_doc["children"]:
            if child_info["componentType"]["code"] == "BARE_MODULE":
                if child_info["component"] == None:
                    return False
                else:
                    child_doc = super().getCompFromProdDB(child_info["component"]["code"])
                    for child_child_info in child_doc["children"]:
                        if child_child_info["componentType"]["code"] == "FE_CHIP":
                            if child_child_info["component"] == None:
                                return False
                            if child_child_info["component"]["dummy"]:
                                return False

        return True

    # public
    def download_QC_info(self, component_id):

        if not os.path.exists(IF_DIR):
            os.makedirs(IF_DIR)
        do_filepath = IF_DIR + '/doing_download.txt'
        with open(do_filepath, 'w') as f:
            f.write('doing now')

        module_cpt_doc = self.pd_client.get("getComponentType",json={"id":"5bb1e1bef981520009c54bc5"})
        self.__downloadStagesAndTests(module_cpt_doc)

        d_module = []
        if component_id != None:
            logger.info("Module: " + component_id)
            d = super().getCompFromProdDB(component_id)
            if self.__checkTypeAndSerialNumber(d):
                d_module.append(d["serialNumber"])
        else:
            doc = self.pd_client.get('listComponents', json={"project":"P","componentType": ["MODULE"]})
            for d in doc:
                if self.__checkTypeAndSerialNumber(d):
                    d_module.append(d["serialNumber"])

        for m_serial in d_module:
            logger.info("Module: " + m_serial)
            component_list = []
            cpr_component_list = []
            cpr_list = []
            chip_slot_map = {}

            try:
                module_doc = super().getCompFromProdDB(m_serial)
                children_flug = self.__checkFEchips(module_doc)
                if not children_flug:
                    massage = "This module does not have all FE chips.(It actually does not have or has a dummy chip.) Skip downloading...\n"
                    logger.warning(massage)

                else:
                    # download component chip info
                    chip_num = 0
                    for child_info in module_doc["children"]:
                        if child_info["componentType"]["code"] == "BARE_MODULE":
                            baremodule_doc = super().getCompFromProdDB(child_info["component"]["code"])
                            for chip_info in baremodule_doc['children']:
                                if chip_info["componentType"]["code"] == "FE_CHIP":
                                    chip_num += 1
                                    chip_doc = super().getCompFromProdDB(chip_info["component"]["code"])
                                    chip_slot_map[chip_doc["id"]] = chip_info["id"]
                                    massage1 = ("Chip" + str(chip_num) + ":" + str(chip_doc["serialNumber"]))

                                    if super().getCompFromLocalDB(chip_doc["serialNumber"]) != None:
                                        chipId = chip_num
                                        address = self.__query_institution(chip_doc)
                                        chip_info = self.__create_component_doc( doc=chip_doc, address=address, chipId=chipId )
                                        cpr_component_list.append(chip_info)
                                        localdb.component.update({"serialNumber": chip_doc["serialNumber"]}, {"$set": {"proID": chip_doc["code"]}})
                                        massage2 = "(already registered)"
                                        logger.info(massage1 + " " + massage2)
                                    else:
                                        # create component chip doc
                                        chipId = chip_num
                                        address = self.__query_institution(chip_doc)
                                        chip_info = self.__create_component_doc( doc=chip_doc, address=address, chipId=chipId )
                                        cpr_component_list.append(chip_info)
                                        component_list.append(chip_info)
                                        massage2 = "(download this chip info)"
                                        logger.info(massage1 + " " + massage2)

                    if super().getCompFromLocalDB(module_doc["serialNumber"]) != None:
                        localdb.component.update({"serialNumber": module_doc["serialNumber"]}, {"$set": {"proID": module_doc["code"]}})
                        massage = "This module has already been registered.\n"
                        logger.info(massage)
                    else:
                        # create module doc
                        address = self.__query_institution(module_doc)
                        module_info = self.__create_component_doc(doc=module_doc,address=address,children=chip_num)
                        component_list.append(module_info)
                        logger.info("Download this module info\n")

                    if not component_list == []:
                        for component_item in component_list:
                            try:
                                localdb.component.insert(component_item)
                            except:
                                logger.info("This module's serial number has been changed for some reason.\n")
                                old_component_name = localdb.component.find_one({"_id":ObjectId(component_item["_id"])})["name"]
                                localdb.component.update({"_id":ObjectId(component_item["_id"])}, {"$set": {"name": component_item["name"], "serialNumber": component_item["serialNumber"], "proID": component_item["proID"]}})
                                localdb.componentTestRun.update_many({"name":old_component_name}, {"$set": {"name": component_item["name"]}})
                                if component_item["componentType"] == "front-end_chip":
                                    localdb.chip.update({"_id":old_component_name}, {"$set": {"name": component_item["name"]}})


                    # create cpr doc
                    localdb_module_doc = super().getCompFromLocalDB(module_doc["serialNumber"])
                    for chip_info in cpr_component_list:
                        if chip_info["componentType"] != "module":
                            localdb_child_doc = super().getCompFromLocalDB(chip_info["serialNumber"])
                            cpr_info = self.__create_cpr_doc( localdb_module_doc, localdb_child_doc, chip_slot_map )
                            if super().getCprFromLocalDB(str(localdb_module_doc["_id"]),str(localdb_child_doc["_id"])) == None:
                                cpr_list.append(cpr_info)
                                logger.info("Module & Chip relation is registered.\n")

                    if not cpr_list == []:
                        localdb.childParentRelation.insert_many(cpr_list)

                    # create QC status doc for a module
                    ex_doc = userdb.QC.status.find_one({"proddbVersion":proddbv})
                    QC_docs = self.__create_module_status_doc(module_doc)
                    if localdb.QC.module.status.find_one( {"component": module_doc["id"], "proddbVersion":proddbv} ) == None:
                        massage = "Created QC status doc.\n"
                        logger.info(massage)
                        localdb.QC.module.status.insert_one( QC_docs )

            except:
                logger.info("Something is wrong. Skip downloading this module.")

        logger.info("Finished!!\n")

        if os.path.exists(IF_DIR + '/doing_download.txt'):
             os.remove(IF_DIR + '/doing_download.txt')
