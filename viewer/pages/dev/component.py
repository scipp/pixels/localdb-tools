def setCount():  # TODO
    docs = {}
    query = {"resultId": str(this_tr["_id"])}
    thisRunInLocal = localdb.localdb.find_one(query)
    if thisRunInLocal:
        docs = thisRunInLocal["count"]
    else:
        writeDat(str(this_tr["_id"]))
        docs = {}
        if DOROOT:
            root.uuid = str(session.get("uuid", "localuser"))
            docs = root.countPix(run.get("test_type"), session["plotList"])
        document = {"resultId": str(this_tr["_id"]), "count": docs}
        localdb.localdb.insert(document)
    return docs


def setPlots(i_oid, i_col, i_tr_oid):  # TODO
    docs = {}
    if i_tr_oid:
        plot_root.retrieveFiles(localdb, i_tr_oid)
        plot_root.plotRoot("./plotting", i_tr_oid)
        docs = plot_root.setRootResult(localdb, i_tr_oid, i_col, i_oid)
    return docs
