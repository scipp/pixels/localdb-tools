from functions.imports import *

error_api = Blueprint("error_api", __name__)

#######################
### route functions ###
#######################

#################################
# display error page if not found
@error_api.app_errorhandler(404)
def page_not_found(e):
    return render_template("404.html"), 404
