# Local DB tools for ITk

It contains,
- DB Viewer (`/viewer`)
- Synchronization tool (`/sync-tool`)
- Archive tool (`/archive-tool`)

# Requirements
- Need `python3` >= 3.4 and its pip.
- Install required packages with root `python3 -m pip install -r ./setting/requirements-pip.txt`.
- Or, install on local `python3 -m pip install -r ./setting/requirements-pip.txt --user`.

* [Setting](#Setting_script)
* [Viewer Application](#Start_up_Viewer_Application)

# Settings for Local DB

`setting/db_server_install.sh` can...

- Confirmation the missing requirements for Local DB and Tools
- Install missing libraries by yum
  - packages written in `requirements-yum.txt`
- Install missing modules by pip3
  - modules written in `requirements-pip.txt`
- Start services
  - MongoDB
- Initialize MongoDB data set (option)
  - clone into /var/lib/mongo and store the previous one to /var/lib/mongo-${today}.tar.gz as backup

## Pre Requirement

- centOS7
- sudo user account

## Usage

```bash
$ cd setting
$ ./db_server_install.sh
```

# Start up Local DB Tools

- Viewer Application: Check detail in `viewer/README.md`
- Synchronization Tool: Check detail in `sync-tool/README.md`
- Archive Tool: Check detail in `archive-tool/README.md`

# Docker

Create a `config.yml` with the appropriate settings and then

```
docker run --rm -p 5000:5000 -v $(pwd)/config.yml:/home/data/config.yml gitlab-registry.cern.ch/yarr/localdb-tools/viewer:master
```

In this case, port 5000 is exposed and the configuration for the server is placed inside the docker image at `/home/data/config.yml`.
